package net.appsys.contentprovidertest

import android.app.AlertDialog
import android.app.LoaderManager
import android.content.*
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.activity_main.*

/**
 * Created by Golovko Dmitry on 27.11.2015. Applied Systems Limited
 */

class MainActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<Cursor>, SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    private val TAG = javaClass.simpleName;

    private val CONTACT_URI = Uri.parse("content://ru.startandroid.providers.AdressBook/contacts");

    private val CONTACT_NAME = "name";
    private val CONTACT_EMAIL = "email";

    var mAdapter: MyCursorAdapter? = null

    var mCurFilter: String? = null;

    private var mSearchView: MySearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main);

        val from = arrayOf("name", "email")
        val to = intArrayOf(android.R.id.text1, android.R.id.text2)

        mAdapter = MyCursorAdapter(this, android.R.layout.simple_list_item_2, null, from, to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)

        val listView = findViewById(android.R.id.list) as ListView
        listView.adapter = mAdapter
        listView.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Delete this row?").setPositiveButton("Yes", { dialogInterface, which ->

                val id = (listView.adapter.getItem(position) as Cursor).getLong(0)
                val uri = ContentUris.withAppendedId(CONTACT_URI, id);
                contentResolver.delete(uri, null, null)
                loaderManager.getLoader<Cursor>(0).onContentChanged()

            }).setNegativeButton("No", null).show()
        }

        newBtn.setOnClickListener {
            val builder = AlertDialog.Builder(this)

            val dialogView = View.inflate(this, R.layout.dialog_add, null);

            val name = dialogView.findViewById(R.id.name) as TextView
            val email = dialogView.findViewById(R.id.email) as TextView

            builder.setTitle("Enter credentials").setPositiveButton("OK", { dialog: DialogInterface?, which: Int ->
                val cv = ContentValues();
                cv.put(CONTACT_NAME, name.text.toString())
                cv.put(CONTACT_EMAIL, email.text.toString())

                val newUri = contentResolver.insert(CONTACT_URI, cv)
                loaderManager.getLoader<Cursor>(0).onContentChanged()
                Log.d(TAG, "insert, result Uri : $newUri")
            }).setNegativeButton("Cancel", null).setView(dialogView).show();
        }

        loaderManager.initLoader(0, null, this);
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        if (menu == null)
            return false

        val item = menu.add("Search");
        item.setIcon(android.R.drawable.ic_menu_search);
        item.setShowAsAction(MenuItemCompat.SHOW_AS_ACTION_ALWAYS)
        mSearchView = MySearchView(this);
        mSearchView?.setOnQueryTextListener(this);
        mSearchView?.setOnCloseListener(this);
        mSearchView?.setIconifiedByDefault(true);

        item.setActionView(mSearchView);

        return true
    }

    override fun onClose(): Boolean {
        if (!TextUtils.isEmpty(mSearchView?.query)) {
            mSearchView?.setQuery(null, true);
        }
        return true;
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        // Don't care about this.
        return true;
    }

    override fun onQueryTextChange(newText: String?): Boolean {

        val newFilter = if (!newText.isNullOrEmpty()) newText else null

        mAdapter?.mFilterString = newFilter

        if (mCurFilter == null && newFilter == null)
            return true;

        if (mCurFilter != null && mCurFilter.equals(newFilter)) {
            return true;
        }

        mCurFilter = newFilter;
        loaderManager.restartLoader(0, null, this);
        return true;
    }

    override fun onLoaderReset(loader: Loader<Cursor>?) {
        mAdapter?.swapCursor(null)
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor>? {
        var select: String? = null

        if (!mCurFilter.isNullOrEmpty())
            select = "($CONTACT_NAME LIKE '%$mCurFilter%') OR ($CONTACT_EMAIL LIKE '%$mCurFilter%')"

        return CursorLoader(this, CONTACT_URI, null, select, null, null);
    }

    override fun onLoadFinished(loader: Loader<Cursor>?, data: Cursor?) {
        mAdapter?.swapCursor(data)
    }

    class MySearchView(context: Context) : SearchView(context) {
        override fun onActionViewCollapsed(): Unit {
            setQuery("", false);
            super.onActionViewCollapsed();
        }
    }

}
