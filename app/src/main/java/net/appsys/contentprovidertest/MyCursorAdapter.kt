package net.appsys.contentprovidertest

import android.content.Context
import android.database.Cursor
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.widget.SimpleCursorAdapter
import android.widget.TextView

/**
 * Created by Golovko Dmitry on 27.11.2015. Applied Systems Limited
 */
class MyCursorAdapter(context: Context?, layoutId: Int, cursor: Cursor?, from: Array<out String>, to: IntArray, flags: Int)
: SimpleCursorAdapter(context, layoutId, cursor, from, to, flags) {

    var mFilterString : String? = null

    override fun setViewText(v: TextView?, text: String?) {
        if (v == null || text == null || mFilterString.isNullOrEmpty() || mFilterString.isNullOrBlank()) {
            super.setViewText(v, text)
        } else {

            val spannableBuilder = SpannableStringBuilder(text)

            var start = substringIndex(text, mFilterString, 0)

            while (start >= 0) {
                spannableBuilder.setSpan(ForegroundColorSpan(v.context.getColor(android.R.color.holo_red_dark)),
                        start, start + (mFilterString as String).length, SpannableStringBuilder.SPAN_EXCLUSIVE_INCLUSIVE)

                start = substringIndex(text, mFilterString, start + (mFilterString as String).length)
            }

            v.text = spannableBuilder
        }
    }

    fun substringIndex(text: String, sub: String?, start: Int): Int {
        return text.indexOf(sub as String, start, true)
    }

}